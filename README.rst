=====================================================
Parkyeri Home Assistant Hass.io Addons
=====================================================

Installation
---------------------
As per `the official instructions`_ on the Home Assistant website, and enter
the following URL on your *Add-on Store* tab:

::

  https://gitlab.com/parkyeri/hassio-addons

Available Add-ons
---------------------

- `smartenergy <http://gitlab.com/parkyeri/hassio-addons/tree/master/smartenergy_2>`_

License
--------

MIT License

Copyright (c) 2018 Parkyeri AS

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

.. _`the official instructions`: https://home-assistant.io/hassio/installing_third_party_addons/
