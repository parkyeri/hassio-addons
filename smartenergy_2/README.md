# Custom Hass.io Add-on: Deniz SmartEnergy

<!-- [![Release][release-shield]][release] ![Project Stage][project-stage-shield] ![Project Maintenance][maintenance-shield]

[![Discord][discord-shield]][discord] [![Community Forum][forum-shield]][forum]

[![Buy me a coffee][buymeacoffee-shield]][buymeacoffee]

[![Support my work on Patreon][patreon-shield]][patreon]
-->
Thermal comfort automator partially based on ASHARE-55 comfort standard.

## About

**Deniz** is a thermal comfort manager that automatically turns devices on or off
depending on your personal preferences, presence within location and weather status.

After an initial setup by the user, the system is supposed to run automatically
alongside HomeAssistant and follow a Device Action Plan that is both predictive
and adaptive. Users can also express their discomfort by pressing the "Discomfort
Expression Button" supplied alongside the system.

[Click here for the full documentation][docs]

## How-To

In the **Config** box below, you are required to supply:
  1. Your _`comfort`_ preference, between 1 (the lowest) and 5 (the highest).
  1. A list _`considered-areas`_, elements of which will be an optimization unit.
  Eg. all devices in `office` will be manipulated to optimize that region's comfort.
  1. A list _`considered-comfort-variables`_, elements of which must be from the set:
      * "air_temperature",
      * "radiant_temperature",
      * "air_speed",
      * "relative_humidity",
      * "presence".
  1. A list _`area-variable-type-name`_, elements of which must be 4 hypen-seperated
  strings picked
      * 1st from `considered-areas`,
      * 2nd from `considered-comfort-variables`,
      * 3rd from ["sensor", "device"] and
      * 4th from HomeAssistant device information
  (ie. device names in the form of `platform.name`).
  1. A non-mandatory _`device-dependencies`_ field for any device requiring
  other devices to work. See below for further example:
      > ```json
      > {
      >   "switch.office_heater": ["switch.central_heater"],
      >   "switch.hitech_heater": [
      >     "switch.central_heater",
      >     "switch.hitech_device"
      >   ]
      > }
      > ```

[buymeacoffee-shield]: https://www.buymeacoffee.com/assets/img/guidelines/download-assets-sm-2.svg
[buymeacoffee]: https://www.buymeacoffee.com/frenck
[discord-shield]: https://img.shields.io/discord/478094546522079232.svg
[discord]: https://discord.me/hassioaddons
[docs]: https://gitlab.com/parkyeri/smarthouse-src/blob/master/controlunit/README.rst
[forum-shield]: https://img.shields.io/badge/community-forum-brightgreen.svg
[forum]: https://community.home-assistant.io/t/community-hass-io-add-on-ide-based-on-cloud9/33810?u=frenck
[hass-ssh]: https://home-assistant.io/addons/ssh/
[maintenance-shield]: https://img.shields.io/maintenance/yes/2019.svg
[ohmyzsh]: http://ohmyz.sh/
[patreon-shield]: https://www.frenck.nl/images/patreon.png
[patreon]: https://www.patreon.com/frenck
[project-stage-shield]: https://img.shields.io/badge/project%20stage-experimental-yellow.svg
[release-shield]: https://img.shields.io/badge/version-v0.8.0-blue.svg
[release]: https://github.com/hassio-addons/addon-ide/tree/v0.8.0
[screenshot]: https://github.com/hassio-addons/addon-ide/raw/master/images/screenshot.png
[zsh]: https://en.wikipedia.org/wiki/Z_shell